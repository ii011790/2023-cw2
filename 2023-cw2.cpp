#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <memory>


// Class representing the character
class Character {
protected:
    std::string name;
    int health;

public:
    //initializes character with name and health
    Character(const std::string& name, int health) : name(name), health(health) {}


    // function to take damage
    void TakeDamage(int damage) {
        health -= damage;
        std::cout << name << " takes " << damage << " damage. Health now: " << health << std::endl;
        if (health<=0) {
            std::cout << name << " has been defeated." << std::endl;
            exit(0);
        }
    }

    //function to heal damage
    void Heal(int amount) {
        health += amount;
        if (health > 100) health = 100;
        std::cout << name << " heals " << amount << " points. Health now: " << health << std::endl;
}
};

//item class
class Item {
private:
    std::string name;
    std::string description;
    int effectValue; 

public:
  virtual ~Item() {} //destructor for polymorphism

    Item(const std::string& name, const std::string& desc, int effectValue) : name(name), description(desc), effectValue(effectValue) {}

    //interaction with items
    void Interact(Character& character) const {
    if (effectValue > 0) {
        std::cout << "Healing " << effectValue << " points." << std::endl;
        character.Heal(effectValue);
    } else if (effectValue < 0) {
        std::cout << "Causing damage of " << -effectValue << " points." << std::endl;
        character.TakeDamage(-effectValue);
    } else {
        std::cout << "Interacting with " << name << ": " << description << std::endl;
    }
}

    const std::string& GetName() const {
        return name;
    }

    const std::string& GetDescription() const {
        return description;
    }

    
    
};

class Inventory {

private:
    std::vector<std::unique_ptr<Item>> items;

public:
    void addItem(std::unique_ptr<Item> item) {
        items.push_back(std::move(item));
    }

    void useItem(const std::string& itemName, Character& target) {
        for (auto& item : items) {
            if (item->GetName() == itemName) {
                item->Interact(target);
                return; 
            }
        }
        std::cout << "Item not found in inventory." << std::endl;
    }

    
    void displayItems() const {
        if (items.empty()) {
            std::cout << "Inventory is empty." << std::endl;
            return;
        }
        std::cout << "Inventory items:" << std::endl;
        for (const auto& item : items) {
            std::cout << "- " << item->GetName() << std::endl;
        }
    }

    bool hasItem(const std::string& itemName) const {
        for (const auto& item : items) {
            if (item->GetName() == itemName) {
                return true;
            }
        }
        return false;
    }

    std::unique_ptr<Item> removeItem(const std::string& itemName) {
        for (auto it = items.begin(); it != items.end(); ++it) {
            if ((*it)->GetName() == itemName) {
                std::unique_ptr<Item> item = std::move(*it); 
                items.erase(it); 
                return item;
            }
        }
        return nullptr; 
    }
    
};




class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<std::unique_ptr<Item>> items;

public:
    Room(const std::string& desc) : description(desc) {}

    void AddItem(std::unique_ptr<Item> item) {
        items.push_back(std::move(item));
    }

    void DisplayItems() const {
      if (items.empty()) {
        std::cout << "There are no items in this room." << std::endl;
     } else {
        std::cout << "Items in the room:" << std::endl;
        for (const auto& item : items) {
            std::cout << "- " << item->GetName() << std::endl;
        }
    }
    }

    std::unique_ptr<Item> TakeItem(const std::string& itemName) {
        for (auto it = items.begin(); it != items.end(); ++it) {
            if ((*it)->GetName() == itemName) {
                std::unique_ptr<Item> item = std::move(*it);
                items.erase(it);
                return item; // Transfers ownership
            }
        }
        return nullptr;
    }

    void AddExit(const std::string& direction, Room* room) {
        exits[direction] = room;
    }

    const std::string& GetDescription() const {
        return description;
    }

    Room* GetExit(const std::string& direction) {
        auto it = exits.find(direction);
        if (it != exits.end()) {
            return it->second;
        }
        return nullptr;
    }

    const std::vector<std::unique_ptr<Item>>& GetItems() const {
        return items;
    }

   
    
};



class Player : public Character {
private:
    Inventory inventory;
    Room* location;

public:
    void pickUpItem(const std::string& itemName) {
      if (!location) {
              std::cout << "You are nowhere. Cannot pick up items." << std::endl;
              return;
          }

      if (itemName == "treasure" && !inventory.hasItem("theCrown")) {
          std::cout << "You are not worthy of this treasure. Only the king has access to this treasure." << std::endl;
          return;
      }
          std::unique_ptr<Item> item = location->TakeItem(itemName);
          if (item) {
              inventory.addItem(std::move(item));
              std::cout << itemName << " picked up." << std::endl;
          } else {
              std::cout << "No item named " << itemName << " found." << std::endl;
          }
      }
    

    void useItem(const std::string& itemName) {
        inventory.useItem(itemName, *this); 
    }

    
    void showInventory() const {
        inventory.displayItems();
    }

    Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

    void SetLocation(Room* room) {
        location = room;
        std::cout << "Current Location: " << location->GetDescription() << std::endl;
        location->DisplayItems();
    }

    Room* GetLocation() const {
        return location;
    }

    void AdjustHealth(int amount) {
        health += amount;
        if (health > 100) health = 100; // 100 is max health
        if (health < 0) health = 0; // Health shouldn't drop below 0
        std::cout << "Current health: " << health << std::endl;
    }

    void dropItem(const std::string& itemName) {
    if (!location) {
        std::cout << "You are nowhere. Cannot drop items." << std::endl;
        return;
    }
    std::unique_ptr<Item> item = inventory.removeItem(itemName);
    if (item) {
        std::cout << itemName << " dropped." << std::endl;
        location->AddItem(std::move(item)); 
    } else {
        std::cout << "No item named " << itemName << " in inventory." << std::endl;
    }
}
};

class Area {
private:
    std::map<std::string, Room*> rooms;

public:
    ~Area() {
        for (auto& pair : rooms) {
            delete pair.second;
        }
    }

    void AddRoom(const std::string& name, Room* room) {
        rooms[name] = room;
    }

    Room* GetRoom(const std::string& name) {
        auto it = rooms.find(name);
        if (it != rooms.end()) {
            return it->second;
        }
        return nullptr;
    }

    void LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    std::string line;

    if (!file) {
        std::cerr << "Failed to open " << filename << std::endl;
        return;
    }

    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string roomName, connectedRoomName, direction;
        if (!(iss >> roomName >> connectedRoomName >> direction)) {
            std::cerr << "Error reading line: " << line << std::endl;
            break; 
        }

        Room* room1 = GetRoom(roomName);
        if (!room1) {
            room1 = new Room(roomName);
            AddRoom(roomName, room1);
        }

        Room* room2 = GetRoom(connectedRoomName);
        if (!room2) {
            room2 = new Room(connectedRoomName);
            AddRoom(connectedRoomName, room2);
        }

        room1->AddExit(direction, room2);
    }
}

};

class CommandInterpreter {
private:
    Player* player;

public:
    CommandInterpreter(Player* p) : player(p) {}

 void interpretCommand(const std::string& command) {
        std::istringstream stream(command);
        std::string action;
        stream >> action;
   
        
        if (action == "look") {
            std::cout << "Current Location: " << player->GetLocation()->GetDescription() << std::endl;
            player->GetLocation()->DisplayItems();
        } else if (action == "move") {
            std::string direction;
            stream >> direction;
            Room* nextRoom = player->GetLocation()->GetExit(direction);
            if (nextRoom != nullptr) {
                player->SetLocation(nextRoom);
                std::cout << "You move to " << nextRoom->GetDescription() << "." << std::endl;
            } else {
                std::cout << "There's no way to go \"" << direction << "\" from here." << std::endl;
            }
        } else if (action == "interact") {
            std::string itemName;
            stream >> itemName;
            auto& items = player->GetLocation()->GetItems();
            bool found = false;
            for (auto& item : items) {
                if (item->GetName() == itemName) {
                    item->Interact(*player); 
                    found = true;
                    break;
                }
            }
            if (!found) {
                std::cout << "There's no item named \"" << itemName << "\" here." << std::endl;
            }
        } else if (action == "pickup") {
            std::string itemName;
            stream >> itemName;
            player->pickUpItem(itemName);
        } else if (action == "use") {
            std::string itemName;
            stream >> itemName;
            player->useItem(itemName);
        } else if (action == "inventory") {
            player->showInventory();
        }
          else if (action == "drop") {
              std::string itemName;
              stream >> itemName;
              if (itemName.empty()) {
                  std::cout << "Specify an item to drop." << std::endl;
              } else {
                  player->dropItem(itemName);
              }
          }
        
        
        else if (action == "quit") {
            std::cout << "Quitting game..." << std::endl;
            exit(0);
        } else {
            std::cout << "Unknown command: " << action << ". Try again." << std::endl;
        }
    }
};

int main() {
    Area gameWorld;
   
    gameWorld.LoadMapFromFile("game_map.txt");

  std::unique_ptr<Item> mysteriousBook = std::make_unique<Item>("mysteriousBook", "Only he who wields the crown may indugle in the treasure...", 0);
  std::unique_ptr<Item> theCrown = std::make_unique<Item>("theCrown", "The legendary crown of the ancient kings.", 0);
  std::unique_ptr<Item> treasure = std::make_unique<Item>("treasure", "A vast treasure that sparkles with a thousand colors.", 0);


    Player player("Hero", 100);

   
   
    Room* courtyard = gameWorld.GetRoom("courtyard");
    if (courtyard) {
        std::unique_ptr<Item> potion = std::make_unique<Item>("goldenFlask", "A flask containing golden liquid.", 50);
        courtyard->AddItem(std::move(potion));
    }
  
    Room* throneRoom = gameWorld.GetRoom("throneRoom");
    if (throneRoom) {
        std::unique_ptr<Item> trap = std::make_unique<Item>("suspiciousFruit", "A suspicious looking fruit.", -80);
        throneRoom->AddItem(std::move(trap));
    }

   
    Room* ancientLibrary = gameWorld.GetRoom("ancientLibrary");
    if (ancientLibrary) {
        ancientLibrary->AddItem(std::move(mysteriousBook));
    } else {
        std::cerr << "ancientLibrary not found." << std::endl;
    }

    Room* kingsQuarters = gameWorld.GetRoom("kingsQuarters");
    if (kingsQuarters) {
        kingsQuarters->AddItem(std::move(theCrown));
    } else {
        std::cerr << "kingsQuarters not found." << std::endl;
    }

    Room* treasureRoom = gameWorld.GetRoom("treasureRoom");
    if (treasureRoom) {
        treasureRoom->AddItem(std::move(treasure));
    } else {
        std::cerr << "treasureRoom not found." << std::endl;
    }


   
    if (courtyard) {
        player.SetLocation(courtyard);
    } else {
        std::cerr << "Start room not found. Check your game_map.txt file." << std::endl;
        return 1;
    }

    CommandInterpreter interpreter(&player);

    std::cout << "Welcome to the Adventure Game! Type 'look' to see where you are, 'move' followed by a direction to move (north, south, east, or west), 'interact' followed by an item name to interact with an item, 'pickup' to add the item to your inventory, 'drop' followed by the item to drop it, 'inventory' to check your inventory, 'use' followed by the item name to use an item in your inventory, or 'quit' to exit the game. Can you find the treasure hidden within?" << std::endl;

    std::string command;
    while (true) {
        std::cout << "> ";
        std::getline(std::cin, command);
        interpreter.interpretCommand(command);
    }

    return 0;
}



